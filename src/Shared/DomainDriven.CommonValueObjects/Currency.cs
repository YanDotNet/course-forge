﻿using DomainDriven.Domain;

namespace DomainDriven.CommonValueObjects;

public class Currency : ValueObject
{
    public string Name { get; private set; }

    public Currency(string name)
    {
        Name = name;
    }

    protected override IEnumerable<object?> GetEqualityComponents()
    {
        yield return Name;
    }
}