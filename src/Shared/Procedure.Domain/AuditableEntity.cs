namespace Procedure.Domain;

public abstract class AuditableEntity : Entity
{
    public DateTimeOffset CreatedAt { get; set; } = DateTimeOffset.Now;
    
    public DateTimeOffset UpdatedAt { get; set; } = DateTimeOffset.Now;
}