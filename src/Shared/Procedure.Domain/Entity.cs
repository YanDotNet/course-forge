﻿using System.ComponentModel.DataAnnotations;

namespace Procedure.Domain;

public abstract class Entity
{
    [Key] 
    public Guid Id { get; set; } = Guid.NewGuid();
}