using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Procedure.Application.Models;
using Procedure.Application.Repositories;
using Procedure.Application.Wrappers;
using Procedure.Domain;

namespace Procedure.Infrastructure.Persistence.EF.Repositories
{
    public class EfReadonlyRepository<T> : IQueryRepository<T> where T : Entity
    {
        private readonly IMapper _mapper;
        private readonly AppReadonlyDbContext _dbContext;

        public EfReadonlyRepository(IMapper mapper, AppReadonlyDbContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public virtual async Task<T?> GetByIdAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var keyValues = new object[] { id };
            return await _dbContext.Set<T>().FindAsync(keyValues, cancellationToken);
        }

        public async Task<IReadOnlyList<T>> ListAllAsync(CancellationToken cancellationToken = default)
        {

            return await _dbContext.Set<T>().ToListAsync(cancellationToken);
        }

        public async Task<IReadOnlyList<TRes>> ListAllAsync<TRes>(CancellationToken cancellationToken = default)
        {
            return await _dbContext.Set<T>()
                .ProjectTo<TRes>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
        }

        public async Task<IReadOnlyList<T?>> ListAsync(ISpecification<T> spec,
            CancellationToken cancellationToken = default)
        {
            var specificationResult = ApplySpecification(spec);
            return await specificationResult.ToListAsync(cancellationToken);
        }

        public async Task<IReadOnlyList<TRes>> ListAsync<TRes>(ISpecification<T> spec,
            CancellationToken cancellationToken = default)
        {
            var specificationResult = ApplySpecification(spec);
            return await specificationResult
                .ProjectTo<TRes>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
        }

        public async Task<int> CountAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            var specificationResult = ApplySpecification(spec);
            return await specificationResult.CountAsync(cancellationToken);
        }

        public async Task<bool> ExistAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            var specificationResult = ApplySpecification(spec);
            return await specificationResult.AnyAsync(cancellationToken);
        }

        public async Task<T?> FirstAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            var specificationResult = ApplySpecification(spec);
            return await specificationResult.FirstAsync(cancellationToken);
        }

        public async Task<T?> FirstOrDefaultAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            var specificationResult = ApplySpecification(spec);
            return await specificationResult.FirstOrDefaultAsync(cancellationToken);
        }

        public async Task<TRes?> FirstOrDefaultAsync<TRes>(ISpecification<T> spec,
            CancellationToken cancellationToken = default) where TRes : class
        {
            var specificationResult = ApplySpecification(spec);
            return await specificationResult
                .ProjectTo<TRes>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(cancellationToken);
        }

        public IQueryable<TRes> ListQuery<TRes>(ISpecification<T> spec, CancellationToken cancellationToken = default) where TRes : class
        {
            var specificationResult = ApplySpecification(spec);
            return specificationResult
                .ProjectTo<TRes>(_mapper.ConfigurationProvider);
        }

        public async Task<PageableData<TRes>> ListPaginated<TRes>
            (ISpecification<T> spec, BasePageableQuery query) where TRes : class
        {
            var specificationResult = ApplySpecification(spec);
            return new PageableData<TRes>(query)
            {
                List = await specificationResult.Skip((query.PageNum - 1) * query.PageSize).Take(query.PageSize)
                    .ProjectTo<TRes>(_mapper.ConfigurationProvider)
                    .ToListAsync(),
                TotalCount = await specificationResult.CountAsync()
            };
        }

        private IQueryable<T?> ApplySpecification(ISpecification<T> spec)
        {
            var evaluator = new SpecificationEvaluator();
            return evaluator.GetQuery(_dbContext.Set<T>().AsQueryable(), spec);
        }
    }
}
