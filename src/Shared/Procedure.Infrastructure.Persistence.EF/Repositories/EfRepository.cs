using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Procedure.Application.Models;
using Procedure.Application.Repositories;
using Procedure.Application.Wrappers;
using Procedure.Domain;

namespace Procedure.Infrastructure.Persistence.EF.Repositories
{
    public class EfRepository<T> : IAsyncRepository<T> where T : Entity
    {
        private readonly IMapper _mapper;
        protected readonly AppDbContext DbContext;

        public EfRepository(AppDbContext dbContext, IMapper mapper)
        {
            DbContext = dbContext;
            _mapper = mapper;
        }

        public virtual async Task<T?> GetByIdAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var keyValues = new object[] { id };
            return await DbContext.Set<T>().FindAsync(keyValues, cancellationToken);
        }

        public async Task<IReadOnlyList<T>> ListAllAsync(CancellationToken cancellationToken = default)
        {
            return await DbContext.Set<T>().ToListAsync(cancellationToken);
        }

        public async Task<IReadOnlyList<TRes>> ListAllAsync<TRes>(CancellationToken cancellationToken = default)
        {
            return await DbContext.Set<T>()
                .ProjectTo<TRes>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
        }

        public async Task<IReadOnlyList<T?>> ListAsync(ISpecification<T> spec,
            CancellationToken cancellationToken = default)
        {
            var specificationResult = ApplySpecification(spec);
            return await specificationResult.ToListAsync(cancellationToken);
        }

        public async Task<IReadOnlyList<TRes>> ListAsync<TRes>(ISpecification<T> spec,
            CancellationToken cancellationToken = default)
        {
            var specificationResult = ApplySpecification(spec);
            return await specificationResult
                .ProjectTo<TRes>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
        }

        public async Task<int> CountAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            var specificationResult = ApplySpecification(spec);
            return await specificationResult.CountAsync(cancellationToken);
        }

        public async Task<bool> ExistAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            var specificationResult = ApplySpecification(spec);
            return await specificationResult.AnyAsync(cancellationToken);
        }

        public async Task<T> AddAsync(T entity, CancellationToken cancellationToken = default)
        {
            await DbContext.Set<T>().AddAsync(entity, cancellationToken);
            await DbContext.SaveChangesAsync(cancellationToken);

            return entity;
        }

        public async Task<T> AddNotTrackedAsync(T entity, CancellationToken cancellationToken = default)
        {
            await DbContext.Set<T>().AddAsync(entity, cancellationToken);
            await DbContext.SaveChangesAsync(cancellationToken);

            return entity;
        }

        public async Task AddRangeNotTrackedAsync(IEnumerable<T> entity, CancellationToken cancellationToken = default)
        {
            await DbContext.Set<T>().AddRangeAsync(entity, cancellationToken);
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task AddRangeAsync(IEnumerable<T> entity, CancellationToken cancellationToken = default)
        {
            await DbContext.Set<T>().AddRangeAsync(entity, cancellationToken);
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task UpdateAsync(T entity, CancellationToken cancellationToken = default)
        {
            DbContext.Set<T>().Update(entity);
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task UpdateRangeAsync(IEnumerable<T> entity, CancellationToken cancellationToken = default)
        {
            DbContext.Set<T>().UpdateRange(entity);
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task AddOrUpdateAsync(T entity)
        {
            var entry = DbContext.Entry(entity);
            switch (entry.State)
            {
                case EntityState.Detached:
                    await AddAsync(entity);
                    break;
                case EntityState.Modified:
                    await UpdateAsync(entity);
                    break;
                case EntityState.Added:
                    await AddAsync(entity);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public async Task DeleteAsync(T entity, CancellationToken cancellationToken = default)
        {
            DbContext.Set<T>().Remove(entity);
            await DbContext.SaveChangesAsync(cancellationToken);
        }
        public async Task DeleteAsync(List<T> entities, CancellationToken cancellationToken = default)
        {
            DbContext.Set<T>().RemoveRange(entities);
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteAsync(IEnumerable<Guid> ids, CancellationToken cancellationToken = default)
        {
            var entities = await DbContext.Set<T>().Where(x => ids.Contains(x.Id)).ToListAsync(cancellationToken);
            DbContext.Set<T>().RemoveRange(entities);
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var entity = DbContext.Set<T>().FirstOrDefault(x => x.Id == id);
            if (entity == null)
                return;
            DbContext.Set<T>().Remove(entity);
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task<T?> FirstAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            var specificationResult = ApplySpecification(spec);
            return await specificationResult.FirstAsync(cancellationToken);
        }

        public async Task<T?> FirstOrDefaultAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            var specificationResult = ApplySpecification(spec);
            return await specificationResult.FirstOrDefaultAsync(cancellationToken);
        }

        public async Task<TRes?> FirstOrDefaultAsync<TRes>(ISpecification<T> spec,
            CancellationToken cancellationToken = default) where TRes : class
        {
            var specificationResult = ApplySpecification(spec);
            return await specificationResult
                .ProjectTo<TRes>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(cancellationToken);
        }

        public IQueryable<TRes> ListQuery<TRes>(ISpecification<T> spec, CancellationToken cancellationToken = default) where TRes : class
        {
            var specificationResult = ApplySpecification(spec);
            return specificationResult
                .ProjectTo<TRes>(_mapper.ConfigurationProvider);
        }

        public async Task<PageableData<TRes>> ListPaginated<TRes>
            (ISpecification<T> spec, BasePageableQuery query) where TRes : class
        {
            var specificationResult = ApplySpecification(spec);
            return new PageableData<TRes>(query)
            {
                List = await specificationResult.Skip((query.PageNum - 1) * query.PageSize).Take(query.PageSize)
                    .ProjectTo<TRes>(_mapper.ConfigurationProvider)
                    .ToListAsync(),
                TotalCount = await specificationResult.CountAsync()
            };
        }

        private IQueryable<T?> ApplySpecification(ISpecification<T> spec)
        {
            var evaluator = new SpecificationEvaluator();
            return evaluator.GetQuery(DbContext.Set<T>().AsQueryable(), spec);
        }
    }
}
