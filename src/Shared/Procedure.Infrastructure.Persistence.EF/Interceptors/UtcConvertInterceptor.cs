using Microsoft.EntityFrameworkCore.Diagnostics;

namespace Procedure.Infrastructure.Persistence.EF.Interceptors;

public class UtcConvertInterceptor : SaveChangesInterceptor
{
    public override ValueTask<InterceptionResult<int>> SavingChangesAsync(
        DbContextEventData eventData, 
        InterceptionResult<int> result,
        CancellationToken cancellationToken = default)
    {
        var entries = eventData.Context?.ChangeTracker.Entries();

        if (entries is null)
            return base.SavingChangesAsync(eventData, result, cancellationToken);
        
        foreach (var entry in entries)
        {
            foreach (var prop in entry.Properties)
            {
                if (prop.Metadata.ClrType == typeof(DateTimeOffset) ||
                    prop.Metadata.ClrType == typeof(DateTimeOffset?) ||
                    prop.Metadata.ClrType == typeof(DateTime) ||
                    prop.Metadata.ClrType == typeof(DateTime?))
                {
                    prop.CurrentValue = prop.CurrentValue switch
                    {
                        DateTimeOffset dto => dto.ToUniversalTime(),
                        DateTime dt => dt.ToUniversalTime(),
                        _ => prop.CurrentValue
                    };
                }
            }
        }
        
        return base.SavingChangesAsync(eventData, result, cancellationToken);
    }
}