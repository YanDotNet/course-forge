using Microsoft.EntityFrameworkCore;

namespace Procedure.Infrastructure.Persistence.EF;

public abstract class AppReadonlyDbContext : DbContext
{
    protected AppReadonlyDbContext(DbContextOptions options) : base(options)
    {
        // ReSharper disable once VirtualMemberCallInConstructor
        ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
    }
}