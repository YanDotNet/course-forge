using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Procedure.Application.Repositories;
using Procedure.Infrastructure.Persistence.EF.Repositories;

namespace Procedure.Infrastructure.Persistence.EF;

public static class DependencyInjection
{
    public static void ConfigureDbContext<TContext>(this IServiceCollection services,
        Action<DbContextOptionsBuilder> options) where TContext : AppDbContext
    {
        services.AddDbContext<TContext>(options);
        services.AddScoped<AppDbContext>(x => x.GetRequiredService<TContext>());
    }
    
    public static void ConfigureDbContext<TContext>(this IServiceCollection services,
        Action<IServiceProvider, DbContextOptionsBuilder> action) where TContext : AppDbContext
    {
        services.AddDbContext<TContext>(action);
        services.AddScoped<AppDbContext>(x => x.GetRequiredService<TContext>());
    }

    public static void ConfigureReadonlyDbContext<TContext>(this IServiceCollection services,
        Action<DbContextOptionsBuilder> options) where TContext : AppReadonlyDbContext
    {
        services.AddDbContext<TContext>(options);
        services.AddScoped<AppReadonlyDbContext>(x => x.GetRequiredService<TContext>());
    }

    public static void ConfigureRepositories(this IServiceCollection services)
    {
        services.AddScoped(typeof(IQueryRepository<>), typeof(EfReadonlyRepository<>));
        services.AddScoped(typeof(IAsyncRepository<>), typeof(EfRepository<>));
        services.AddScoped(typeof(ICommandRepository<>), typeof(EfRepository<>));
    }
}