using Microsoft.EntityFrameworkCore;

namespace Procedure.Infrastructure.Persistence.EF;

public abstract class AppDbContext : DbContext
{
    protected AppDbContext(DbContextOptions options) : base(options)
    {
    }
}