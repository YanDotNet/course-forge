using Microsoft.Extensions.Configuration;

namespace Procedure.Infrastructure.Masstransit.RabbitMq;

public static class RabbitMqHelper
{
    public static string CreateConnectionString(IConfiguration configuration)
    {
        return
            $"amqp://{configuration["RabbitMQ:UserName"]}:{configuration["RabbitMQ:Password"]}@{configuration["RabbitMQ:HostName"]}:{configuration["RabbitMQ:Port"]}{configuration["RabbitMQ:VirtualHost"]}";
    }
}