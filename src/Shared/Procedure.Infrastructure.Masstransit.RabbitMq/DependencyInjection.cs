﻿using System.Reflection;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Procedure.Infrastructure.Masstransit.RabbitMq;

public static class DependencyInjection
{
    public static void ConfigureMasstransit<TContext>(this IServiceCollection services, IConfiguration configuration, params Assembly[] assemblies) where TContext : DbContext
    {
        services.AddMassTransit(x =>
        {
            x.RegisterConsumersFrom(assemblies);
            
            x.AddEntityFrameworkOutbox<TContext>(o =>
            {
                o.UsePostgres().UseBusOutbox();
            });

            x.UsingRabbitMq((context, cfg) =>
            {
                cfg.Host($"{configuration["RabbitMQ:HostName"]}", configuration["RabbitMQ:VirtualHost"], h =>
                {
                    h.Username(configuration["RabbitMQ:UserName"]);
                    h.Password(configuration["RabbitMQ:Password"]);
                });

                cfg.ConfigureEndpoints(context);
            });
        });
    }
    
    public static void ConfigureMasstransit(this IServiceCollection services, IConfiguration configuration, params Assembly[] assemblies)
    {
        services.AddMassTransit(x =>
        {
            x.RegisterConsumersFrom(assemblies);

            x.UsingRabbitMq((context, cfg) =>
            {
                cfg.Host($"{configuration["RabbitMQ:HostName"]}", configuration["RabbitMQ:VirtualHost"], h =>
                {
                    h.Username(configuration["RabbitMQ:UserName"]);
                    h.Password(configuration["RabbitMQ:Password"]);
                });

                cfg.ConfigureEndpoints(context);
            });
        });
    }
    
    private static void RegisterConsumersFrom(this IRegistrationConfigurator cfg, params Assembly[] consumerAssemblies)
    {
        var consumers = FindConsumers(consumerAssemblies);

        foreach (var consumer in consumers)
        {
            cfg.AddConsumer(consumer);
        }
    }

    private static IEnumerable<Type> FindConsumers(params Assembly[] assemblies)
    {
        var consumerInterfaceType = typeof(IConsumer<>);

        return assemblies.SelectMany(assembly => assembly.GetTypes()
            .Where(t => t.GetInterfaces()
                .Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == consumerInterfaceType)));
    }
}