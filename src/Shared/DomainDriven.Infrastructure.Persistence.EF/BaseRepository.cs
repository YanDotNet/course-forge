using DomainDriven.Domain;
using DomainDriven.Domain.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace DomainDriven.Infrastructure.Persistence.EF;

public class BaseRepository<T> : IRepository<T> where T : AggregateRoot
{
    private readonly DbContext _context;

    public BaseRepository(DbContext context)
    {
        _context = context;
    }

    public async Task<T> Get(Guid id)
    {
        var result = await _context.Set<T>().FirstOrDefaultAsync(x => x.Id == id);

        if (result is null)
            throw new NotFoundException();
        
        return result;
    }

    public void Add(T aggregate)
    {
        _context.Set<T>().Add(aggregate);
    }

    public void Delete(T aggregate)
    {
        _context.Set<T>().Remove(aggregate);
    }
}