﻿using DomainDriven.Application;
using DomainDriven.Application.EventMappers;
using DomainDriven.Domain;
using IntegrationEvents.Contracts;
using Microsoft.EntityFrameworkCore;

namespace DomainDriven.Infrastructure.Persistence.EF;

public class UnitOfWork<TContext> : IUnitOfWork where TContext : DbContext
{
    private readonly TContext _dbContext;
    private readonly IDomainEventPublisher _domainEventPublisher;
    private readonly IIntegrationEventPublisher _integrationEventPublisher;
    private readonly IEventMapper _eventMapper;

    public UnitOfWork(TContext dbContext, IDomainEventPublisher domainEventPublisher,
        IIntegrationEventPublisher integrationEventPublisher, IEventMapper eventMapper)
    {
        _dbContext = dbContext;
        _domainEventPublisher = domainEventPublisher;
        _integrationEventPublisher = integrationEventPublisher;
        _eventMapper = eventMapper;
    }

    public async Task DispatchEventsAsync(CancellationToken cancellationToken = default)
    {
        var aggregateRoots = _dbContext.ChangeTracker.Entries<AggregateRoot>()
            .Where(x => x.Entity.DomainEvents.Any())
            .Select(e => e.Entity)
            .ToList();

        var domainEvents = aggregateRoots.SelectMany(x => x.DomainEvents).ToList();
        aggregateRoots.ForEach(aggregate => aggregate.ClearDomainEvents());
        
        // Публикуем доменные события
        foreach (var domainEvent in domainEvents)
        {
            await _domainEventPublisher.PublishAsync(domainEvent, cancellationToken);
        }
        
        // Публикуем интеграционные события
        foreach (var integrationEvent in domainEvents.SelectMany(domainEvent => _eventMapper.Map(domainEvent)))
        {
            await _integrationEventPublisher.PublishAsync(integrationEvent, cancellationToken);
        }
    }

    public async Task CommitAsync(CancellationToken cancellationToken = default)
    {
        var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);

        try
        {
            await DispatchEventsAsync(cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);
            await transaction.CommitAsync(cancellationToken);
        }
        catch (Exception)
        {
            await transaction.RollbackAsync(cancellationToken);
        }
    }
}