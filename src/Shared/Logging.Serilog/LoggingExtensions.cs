﻿using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace Logging.Serilog;

public static class LoggingExtensions
{
    public static void ConfigureSerilogLogger(this IHostBuilder hostBuilder, string? applicationContext = null, LogEventLevel logEventLevel = LogEventLevel.Error)
    {
        hostBuilder.UseSerilog((ctx, conf) =>
        {
            applicationContext ??= Assembly.GetEntryAssembly()?.ManifestModule.Name.Replace(".dll", "") ??
                                   Assembly.GetCallingAssembly().FullName;

            var serviceName = Environment.GetEnvironmentVariable("SERVICE_NAME");
            logEventLevel = ctx.Configuration.GetValue("Serilog:MinimumLevel:Default", logEventLevel);
            
            conf.MinimumLevel.ControlledBy(new LoggingLevelSwitch(logEventLevel))
                .Enrich.WithProperty("ApplicationContext", applicationContext)
                .Enrich.WithProperty("ServiceName", serviceName)
                .Enrich.FromLogContext()
                .Enrich.WithMachineName()
                .Enrich.WithClientIp()
                .Enrich.WithClientAgent()
                .ReadFrom.Configuration(ctx.Configuration);
        });
    }
}