﻿using System.Text.Json;
using Infrastructure.EF.Auditing.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Infrastructure.EF.Auditing.Extensions
{
    internal static class DbContextExtensions
    {
        /// <summary>
        /// Ensures the automatic auditing history.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="username">User made the action.</param>
        internal static void EnsureAuditHistory(this DbContext context, string username)
        {
            var entries = context.ChangeTracker.Entries()
                .Where(e => !AuditUtilities.IsAuditDisabled(e.Entity.GetType()) &&
                            e.State is EntityState.Added or EntityState.Modified or EntityState.Deleted)
                .ToArray();
           
            foreach (var entry in entries)
            {
                context.Add(entry.AutoHistory(username));
            }
        }

        internal static AuditHistory AutoHistory(this EntityEntry entry, string username)
        {
            var history = new AuditHistory { TableName = entry.Metadata.GetTableName()!, Username = username };

            // Get the mapped properties for the entity type.
            // (include shadow properties, not include navigations & references)
            var properties = entry.Properties.Where(p =>
                !AuditUtilities.IsAuditDisabled(p.EntityEntry.Entity.GetType(), p.Metadata.Name));

            foreach (var prop in properties)
            {
                string propertyName = prop.Metadata.Name;
                if (prop.Metadata.IsPrimaryKey())
                {
                    history.AutoHistoryDetails.NewValues[propertyName] = prop.CurrentValue;
                    continue;
                }

                switch (entry.State)
                {
                    case EntityState.Added:
                        history.RowId = "0";
                        history.Kind = EntityState.Added;
                        history.AutoHistoryDetails.NewValues.Add(propertyName, prop.CurrentValue);
                        break;

                    case EntityState.Modified:
                        history.RowId = entry.PrimaryKey();
                        history.Kind = EntityState.Modified;
                        history.AutoHistoryDetails.OldValues.Add(propertyName, prop.OriginalValue);
                        history.AutoHistoryDetails.NewValues.Add(propertyName, prop.CurrentValue);
                        break;

                    case EntityState.Deleted:
                        history.RowId = entry.PrimaryKey();
                        history.Kind = EntityState.Deleted;
                        history.AutoHistoryDetails.OldValues.Add(propertyName, prop.OriginalValue);
                        break;
                }
            }

            history.Changed = JsonSerializer.Serialize(history.AutoHistoryDetails);

            return history;
        }

        private static string PrimaryKey(this EntityEntry entry)
        {
            var key = entry.Metadata.FindPrimaryKey();

            var values = key?.Properties.Select(property => entry.Property(property.Name).CurrentValue)
                .Where(value => value != null)
                .ToList();

            return string.Join(",", values ?? new List<object?>());
        }
    }
}