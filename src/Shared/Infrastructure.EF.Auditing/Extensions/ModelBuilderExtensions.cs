﻿using Infrastructure.EF.Auditing.Models;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.EF.Auditing.Extensions
{
    /// <summary>
    /// Represents a plugin for Microsoft.EntityFrameworkCore to support automatically recording data changes history.
    /// </summary>
    public static class ModelBuilderExtensions
    {
        /// <summary>
        /// Enables auditing change history.
        /// </summary>
        /// <param name="modelBuilder">The <see cref="ModelBuilder"/> to enable auto history functionality.</param>
        /// <param name="schema"></param>
        /// <returns>The <see cref="ModelBuilder"/> to enable auto history functionality.</returns>
        public static ModelBuilder EnableAuditHistory(this ModelBuilder modelBuilder, string? schema = null)
        {
            modelBuilder.Entity<AuditHistory>().ToTable("AuditHistory", schema).Ignore(t => t.AutoHistoryDetails);
            modelBuilder.Entity<AuditHistory>(b =>
            {
                b.HasKey(c => c.Id);
                b.Property(c => c.RowId).IsRequired().HasMaxLength(128);
                b.Property(c => c.TableName).IsRequired().HasMaxLength(128);
                b.Property(c => c.Username).HasMaxLength(128);
            });

            return modelBuilder;
        }
    }
}