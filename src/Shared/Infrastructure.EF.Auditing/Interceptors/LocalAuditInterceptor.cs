using Infrastructure.EF.Auditing.Extensions;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Security.Auth;

namespace Infrastructure.EF.Auditing.Interceptors;

public class LocalAuditInterceptor : SaveChangesInterceptor
{
    private readonly IAuthContext _authContext;

    public LocalAuditInterceptor(IAuthContext authContext)
    {
        _authContext = authContext;
    }
    
    public override ValueTask<InterceptionResult<int>> SavingChangesAsync(DbContextEventData eventData, InterceptionResult<int> result,
        CancellationToken cancellationToken = new())
    {
        eventData.Context?.EnsureAuditHistory(_authContext.Username);
        return base.SavingChangesAsync(eventData, result, cancellationToken);
    }
}