using Infrastructure.EF.Auditing.Extensions;
using Infrastructure.EF.Auditing.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Security.Auth;

namespace Infrastructure.EF.Auditing.Interceptors;

public class RemoteAuditInterceptor : SaveChangesInterceptor
{
    private readonly IAuthContext _authContext;
    private static readonly Lazy<List<AuditHistory>> AuditHistories = new(new List<AuditHistory>());

    public RemoteAuditInterceptor(IAuthContext authContext)
    {
        _authContext = authContext;
    }
    
    public override ValueTask<InterceptionResult<int>> SavingChangesAsync(DbContextEventData eventData, InterceptionResult<int> result,
        CancellationToken cancellationToken = new())
    {
        var entries = eventData.Context?.ChangeTracker.Entries()
            .Where(e => !AuditUtilities.IsAuditDisabled(e.Entity.GetType()) &&
                        e.State is EntityState.Added or EntityState.Modified or EntityState.Deleted)
            .ToArray();

        if (entries is not null)
        {
            AuditHistories.Value.AddRange(entries.Select(x => x.AutoHistory(_authContext.Username)));
        }
        
        return base.SavingChangesAsync(eventData, result, cancellationToken);
    }

    public override async ValueTask<int> SavedChangesAsync(SaveChangesCompletedEventData eventData, int result,
        CancellationToken cancellationToken = new())
    {
        var httpClient = new HttpClient();
        
        var sendResult = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Post, "auditApp/api/Audit"), cancellationToken);
        
        if (sendResult.IsSuccessStatusCode)
            AuditHistories.Value.Clear();
     
        return await base.SavedChangesAsync(eventData, result, cancellationToken);
    }
}