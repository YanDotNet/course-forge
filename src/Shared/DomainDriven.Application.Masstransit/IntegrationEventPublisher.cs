using IntegrationEvents.Contracts;
using MassTransit;

namespace DomainDriven.Application.Masstransit;

public class IntegrationEventPublisher : IIntegrationEventPublisher
{
    private readonly IPublishEndpoint _publishEndpoint;

    public IntegrationEventPublisher(IPublishEndpoint publishEndpoint)
    {
        _publishEndpoint = publishEndpoint;
    }
    
    public async Task PublishAsync(IIntegrationEvent integrationEvent, CancellationToken cancellationToken = default)
    {
        await _publishEndpoint.Publish((object)integrationEvent, cancellationToken);
    }
}