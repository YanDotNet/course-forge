using System.Reflection;
using DomainDriven.Application.Masstransit.Filters;
using DomainDriven.Domain;
using MassTransit;
using MassTransit.Internals;

namespace DomainDriven.Application.Masstransit;

public static class DependencyInjection
{
    public static void RegisterIntegrationEventHandlers(
        this IBusRegistrationConfigurator configurator, 
        Assembly[] integrationEventHandlerAssemblies)
    {
        var consumers = FindConsumers(integrationEventHandlerAssemblies);

        foreach (var consumer in consumers)
        {
            configurator.AddConsumer(consumer);
        }

        IEnumerable<Type> FindConsumers(params Assembly[] assemblies)
        {
            var requestWithResponseType = typeof(BaseIntegrationEventHandler<>);

            return assemblies.SelectMany(assembly => assembly.GetTypes()
                .Where(t => t.GetInterfaces()
                    .Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == requestWithResponseType)));
        }
    }
    
    public static void ConfigureMasstransitMediatorWithDefaultDecorators(
        this IBusRegistrationConfigurator configurator, 
        Assembly[] useCaseAndDomainEventHandlerAssemblies)
    {
        configurator.AddMediator(cfg =>
        {
            cfg.RegisterApplicationConsumers(useCaseAndDomainEventHandlerAssemblies);
            
            cfg.ConfigureMediator((c, v) =>
            {
                v.UseConsumeFilter(typeof(PublishEventsDomainEventHandlerDecorator<>), c,
                    x => x.Include<IDomainEvent>());
                
                v.UseConsumeFilter(typeof(TransactionalRequestHandlerDecorator<>), c,
                    x => x.Include<IRequest>());
                
                v.UseConsumeFilter(typeof(TransactionalRequestHandlerDecorator<>), c,
                    x => x.Include(type => type.HasInterface(typeof(IRequest<>))));
            });
        });
    } 
    
    public static void ConfigureMasstransitMediatorWithDefaultDecorators(
        this IBusRegistrationConfigurator configurator, 
        Assembly[] useCaseAndDomainEventHandlerAssemblies,
        Action<IMediatorRegistrationContext,IMediatorConfigurator> mediatorConfigure)
    {
        configurator.AddMediator(cfg =>
        {
            cfg.RegisterApplicationConsumers(useCaseAndDomainEventHandlerAssemblies);
            
            cfg.ConfigureMediator((c, v) =>
            {
                v.UseConsumeFilter(typeof(PublishEventsDomainEventHandlerDecorator<>), c,
                    x => x.Include<IDomainEvent>());
                
                v.UseConsumeFilter(typeof(TransactionalRequestHandlerDecorator<>), c,
                    x => x.Include<IRequest>());
                
                v.UseConsumeFilter(typeof(TransactionalRequestHandlerDecorator<>), c,
                    x => x.Include(type => type.HasInterface(typeof(IRequest<>))));

                mediatorConfigure(c, v);
            });
        });
    }
    
    public static void ConfigureMasstransitMediator(
        this IBusRegistrationConfigurator configurator, 
        Assembly[] useCaseAndDomainEventHandlerAssemblies,
        Action<IMediatorRegistrationContext,IMediatorConfigurator> mediatorConfigure)
    {
        configurator.AddMediator(cfg =>
        {
            cfg.RegisterApplicationConsumers(useCaseAndDomainEventHandlerAssemblies);
            
            cfg.ConfigureMediator(mediatorConfigure);
        });
    }
    
    private static void RegisterApplicationConsumers(this IMediatorRegistrationConfigurator cfg,
        params Assembly[] consumerAssemblies)
    {
        var consumers = FindConsumers(consumerAssemblies);

        foreach (var consumer in consumers)
        {
            cfg.AddConsumer(consumer);
        }

        IEnumerable<Type> FindConsumers(params Assembly[] assemblies)
        {
            var requestType = typeof(BaseRequestHandler<>);
            var requestWithResponseType = typeof(BaseRequestHandler<,>);
            var domainEventHandlerType = typeof(BaseDomainEventHandler<>);

            return assemblies.SelectMany(assembly => assembly.GetTypes()
                .Where(t => t.GetInterfaces()
                    .Any(i => i.IsGenericType && (i.GetGenericTypeDefinition() == requestWithResponseType ||
                                                  i.GetGenericTypeDefinition() == requestType || 
                                                  i.GetGenericTypeDefinition() == domainEventHandlerType))));
        }
    }
}