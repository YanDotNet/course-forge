using MassTransit.Mediator;

namespace DomainDriven.Application.Masstransit;

public class ServiceBusMediator : IServiceBus
{
    private readonly IScopedMediator _mediator;

    public ServiceBusMediator(IScopedMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request,
        CancellationToken cancellationToken = default) where TResponse : class
    {
        var client = _mediator.CreateRequestClient<IRequest<TResponse>>();
        cancellationToken.ThrowIfCancellationRequested();
        var response = await client.GetResponse<TResponse>(request, cancellationToken);
        return response.Message;
    }

    public async Task SendAsync(IRequest request, CancellationToken cancellationToken = default)
    {
        await _mediator.Send(request, cancellationToken);
    }
}