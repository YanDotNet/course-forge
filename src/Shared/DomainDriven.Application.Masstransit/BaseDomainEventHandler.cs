using DomainDriven.Domain;
using MassTransit;

namespace DomainDriven.Application.Masstransit;

public abstract class BaseDomainEventHandler<TDomainEvent> : IDomainEventHandler<TDomainEvent>, IConsumer<TDomainEvent> where TDomainEvent : class, IDomainEvent
{
    public abstract Task HandleAsync(TDomainEvent domainEvent, CancellationToken cancellationToken = default);

    public async Task Consume(ConsumeContext<TDomainEvent> context)
    {
        await HandleAsync(context.Message, context.CancellationToken);
    }
}