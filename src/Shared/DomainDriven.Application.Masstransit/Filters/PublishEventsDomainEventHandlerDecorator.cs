using DomainDriven.Domain;
using MassTransit;

namespace DomainDriven.Application.Masstransit.Filters;

public class PublishEventsDomainEventHandlerDecorator<TDomainEvent> : IFilter<ConsumeContext<TDomainEvent>> where TDomainEvent : class, IDomainEvent
{
    private readonly IUnitOfWork _unitOfWork;

    public PublishEventsDomainEventHandlerDecorator(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    public async Task Send(ConsumeContext<TDomainEvent> context, IPipe<ConsumeContext<TDomainEvent>> next)
    {
        await next.Send(context);
        await _unitOfWork.DispatchEventsAsync(context.CancellationToken);
    }

    public void Probe(ProbeContext context)
    { }
}