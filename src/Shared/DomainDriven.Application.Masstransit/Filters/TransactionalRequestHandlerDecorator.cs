using DomainDriven.Domain;
using MassTransit;

namespace DomainDriven.Application.Masstransit.Filters;

public class TransactionalRequestHandlerDecorator<TRequest> : IFilter<ConsumeContext<TRequest>> where TRequest : class, IRequest
{
    private readonly IUnitOfWork _unitOfWork;

    public TransactionalRequestHandlerDecorator(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    public async Task Send(ConsumeContext<TRequest> context, IPipe<ConsumeContext<TRequest>> next)
    {
        await next.Send(context);
        await _unitOfWork.CommitAsync(context.CancellationToken);
    }

    public void Probe(ProbeContext context)
    { }
}