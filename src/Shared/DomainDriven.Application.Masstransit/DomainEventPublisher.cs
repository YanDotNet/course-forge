using DomainDriven.Domain;
using MassTransit.Mediator;

namespace DomainDriven.Application.Masstransit;

public class DomainEventPublisher : IDomainEventPublisher
{
    private readonly IScopedMediator _mediator;

    public DomainEventPublisher(IScopedMediator mediator)
    {
        _mediator = mediator;
    }
    
    public async Task PublishAsync(IDomainEvent domainEvent, CancellationToken cancellationToken = default)
    {
        await _mediator.Publish((object)domainEvent, cancellationToken);
    }
}