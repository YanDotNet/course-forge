using MassTransit;

namespace DomainDriven.Application.Masstransit;

public abstract class BaseRequestHandler<TRequest, TResponse> : IHandler<TRequest, TResponse>, IConsumer<TRequest>
    where TResponse : class where TRequest : class, IRequest<TResponse>
{
    public abstract Task<TResponse> HandleAsync(TRequest command, CancellationToken cancellationToken = default);

    public async Task Consume(ConsumeContext<TRequest> context)
    {
        var messageResult = await HandleAsync(context.Message);
        await context.RespondAsync(messageResult);
    }
}

public abstract class BaseRequestHandler<TRequest> : IHandler<TRequest>, IConsumer<TRequest> 
    where TRequest : class, IRequest
{
    public async Task Consume(ConsumeContext<TRequest> context)
    {
        await HandleAsync(context.Message);
    }

    public abstract Task HandleAsync(TRequest command, CancellationToken cancellationToken = default);
}