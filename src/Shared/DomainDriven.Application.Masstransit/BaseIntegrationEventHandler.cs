using IntegrationEvents.Contracts;
using MassTransit;

namespace DomainDriven.Application.Masstransit;

public abstract class BaseIntegrationEventHandler<TIntegrationEvent> : IIntegrationEventHandler<TIntegrationEvent>,
    IConsumer<TIntegrationEvent> where TIntegrationEvent : class, IIntegrationEvent
{
    public abstract Task HandleAsync(TIntegrationEvent integrationEvent, CancellationToken cancellationToken = default);

    public async Task Consume(ConsumeContext<TIntegrationEvent> context)
    {
        await HandleAsync(context.Message, context.CancellationToken);
    }
}