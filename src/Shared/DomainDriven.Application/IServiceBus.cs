namespace DomainDriven.Application;

public interface IServiceBus
{
    Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request, CancellationToken cancellationToken = default)
        where TResponse : class;

    Task SendAsync(IRequest request, CancellationToken cancellationToken = default);
}