using DomainDriven.Domain;
using IntegrationEvents.Contracts;

namespace DomainDriven.Application.EventMappers;

public interface IEventMapper
{
    IEnumerable<IIntegrationEvent> Map(IDomainEvent domainEvent);
}