namespace DomainDriven.Application;

public interface IRequest
{
    
}

public interface IRequest<TResponse> where TResponse : class
{
    
}