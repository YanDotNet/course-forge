using DomainDriven.Domain;

namespace DomainDriven.Application;

public interface IDomainEventHandler<in TDomainEvent> where TDomainEvent : class, IDomainEvent
{
    Task HandleAsync(TDomainEvent domainEvent, CancellationToken cancellationToken = default);
}