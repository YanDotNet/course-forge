namespace DomainDriven.Application;

public interface IHandler<in TCommand> where TCommand : IRequest
{
    Task HandleAsync(TCommand command, CancellationToken cancellationToken = default);
}

public interface IHandler<in TCommand, TResult> where TCommand : class, IRequest<TResult> where TResult : class
{
    Task<TResult> HandleAsync(TCommand command, CancellationToken cancellationToken = default);
}