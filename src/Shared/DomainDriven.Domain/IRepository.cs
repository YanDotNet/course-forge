namespace DomainDriven.Domain;

public interface IRepository<T> where T : AggregateRoot
{
    Task<T> Get(Guid id);

    void Add(T aggregate);

    void Delete(T aggregate);
}