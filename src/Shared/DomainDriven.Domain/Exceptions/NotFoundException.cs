namespace DomainDriven.Domain.Exceptions;

public class NotFoundException : Exception
{
    public NotFoundException() : base("not.found")
    {
        
    }

    public NotFoundException(string message) : base(message)
    {
        
    }
}