namespace DomainDriven.Domain;

public interface IUnitOfWork
{
    Task DispatchEventsAsync(CancellationToken cancellationToken = default);
    Task CommitAsync(CancellationToken cancellationToken = default);
}