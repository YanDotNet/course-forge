using Procedure.Application.Models;

namespace Procedure.Application.Wrappers
{
    public class PageableData<T> where T : class
    {
        public IEnumerable<T> List { get; set; }
        public int PageNum { get; set; }
        public int TotalCount { get; set; }
        public int PageSize { get; set; }

        public PageableData(BasePageableQuery query)
        {
            PageNum = query.PageNum;
            PageSize = query.PageSize;
        }

        public PageableData( int pageNum = 1, int pageSize = 20)
        {
            PageNum = pageNum;
            PageSize = pageSize;
        }
        
        public PageableData(IQueryable<T> queryableSet, int pageNum, int pageSize = 10)
        {
            PageSize = pageSize;
            PageNum = pageNum;
            List = queryableSet.Skip((PageNum - 1) * pageSize).Take(pageSize);
            TotalCount = queryableSet.AsQueryable().Count();
        }
        
        public PageableData(IQueryable<T> queryableSet, BasePageableQuery query)
        {
            PageSize = query.PageSize;
            PageNum = query.PageNum;
            List = queryableSet.Skip((PageNum - 1) * query.PageSize).Take(query.PageSize);
            TotalCount = queryableSet.AsQueryable().Count();
        }
    }
}