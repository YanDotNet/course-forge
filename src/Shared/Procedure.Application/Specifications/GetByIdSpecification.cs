using Ardalis.Specification;
using Procedure.Domain;

namespace Procedure.Application.Specifications;

public sealed class GetByIdSpecification<T> : Specification<T> where T : Entity
{
    public GetByIdSpecification(Guid id)
    {
        Query.Where(x => x.Id == id);
    }
}