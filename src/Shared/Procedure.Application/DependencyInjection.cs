using Microsoft.Extensions.DependencyInjection;
using Procedure.Application.Services;

namespace Procedure.Application;

public static class DependencyInjection
{
    public static void ConfigureServices(this IServiceCollection services)
    {
        services.AddScoped(typeof(IQueryService<>), typeof(QueryService<>));
        services.AddScoped(typeof(ICommandService<>), typeof(CommandService<>));
    }
}