namespace Procedure.Application.Models
{
    public class BasePageableQuery
    {
        public int PageNum { get; set; } = 1;
        public int PageSize { get; set; } = 20;
    }
}