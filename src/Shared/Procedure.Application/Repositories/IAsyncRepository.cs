using Procedure.Domain;

namespace Procedure.Application.Repositories
{
    public interface IAsyncRepository<T> : IQueryRepository<T>, ICommandRepository<T>
        where T : Entity
    {
    }
}
