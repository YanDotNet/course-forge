using Ardalis.Specification;
using Procedure.Application.Models;
using Procedure.Application.Wrappers;
using Procedure.Domain;

namespace Procedure.Application.Repositories
{
    public interface IQueryRepository<T> where T : Entity
    {
        Task<T?> GetByIdAsync(Guid id, CancellationToken cancellationToken = default);
        Task<IReadOnlyList<T>> ListAllAsync(CancellationToken cancellationToken = default);
        Task<IReadOnlyList<TRes>> ListAllAsync<TRes>(CancellationToken cancellationToken = default);
        Task<IReadOnlyList<T?>> ListAsync(ISpecification<T> spec, CancellationToken cancellationToken = default);

        Task<IReadOnlyList<TRes>> ListAsync<TRes>(ISpecification<T> spec,
            CancellationToken cancellationToken = default);

        Task<int> CountAsync(ISpecification<T> spec, CancellationToken cancellationToken = default);
        Task<bool> ExistAsync(ISpecification<T> spec, CancellationToken cancellationToken = default);
        Task<T?> FirstAsync(ISpecification<T> spec, CancellationToken cancellationToken = default);
        Task<T?> FirstOrDefaultAsync(ISpecification<T> spec, CancellationToken cancellationToken = default);
        Task<TRes?> FirstOrDefaultAsync<TRes>(ISpecification<T> spec, CancellationToken cancellationToken = default) where TRes : class;
        IQueryable<TRes> ListQuery<TRes>(ISpecification<T> spec, CancellationToken cancellationToken = default) where TRes : class;

        Task<PageableData<TRes>> ListPaginated<TRes>
            (ISpecification<T> spec, BasePageableQuery query) where TRes : class;
    }
}
