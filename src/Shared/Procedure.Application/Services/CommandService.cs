using AutoMapper;
using Procedure.Application.Repositories;
using Procedure.Domain;

namespace Procedure.Application.Services;

public class CommandService<T> : ICommandService<T> where T : Entity
{
    private readonly IAsyncRepository<T> _repository;
    private readonly IMapper _mapper;

    public CommandService(IAsyncRepository<T> repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }
    
    public async Task<Guid> AddAsync<TDto>(TDto dto, CancellationToken cancellationToken = default) where TDto : class
    {
        var entity = _mapper.Map<T>(dto);

        var result = await _repository.AddAsync(entity, cancellationToken);

        return result.Id;
    }

    public async Task AddRangeAsync<TDto>(ICollection<TDto> dtos, CancellationToken cancellationToken = default) where TDto : class
    {
        var entities = _mapper.Map<ICollection<T>>(dtos);
        await _repository.AddRangeAsync(entities, cancellationToken);
    }

    public async Task UpdateAsync<TDto>(TDto dto, CancellationToken cancellationToken = default) where TDto : class
    {
        var entity = _mapper.Map<T>(dto);
        await _repository.UpdateAsync(entity, cancellationToken);
    }

    public async Task UpdateAsync<TDto>(ICollection<TDto> dtos, CancellationToken cancellationToken = default) where TDto : class
    {
        var entities = _mapper.Map<ICollection<T>>(dtos);
        await _repository.UpdateRangeAsync(entities, cancellationToken);
    }

    public async Task RemoveAsync(Guid id, CancellationToken cancellationToken = default)
    {
        await _repository.DeleteAsync(id, cancellationToken);
    }

    public async Task RemoveRangeAsync(ICollection<Guid> ids, CancellationToken cancellationToken = default)
    {
        await _repository.DeleteAsync(ids, cancellationToken);
    }
}