using Procedure.Domain;

namespace Procedure.Application.Services;

public interface IQueryService<T> where T : Entity
{
    Task<TResult?> GetById<TResult>(Guid id, CancellationToken cancellationToken = default) where TResult : class;
    Task<IReadOnlyCollection<TResult>> GetAll<TResult>(CancellationToken cancellationToken = default) where TResult : class;
}