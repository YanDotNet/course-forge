using AutoMapper;
using Procedure.Application.Repositories;
using Procedure.Application.Specifications;
using Procedure.Domain;

namespace Procedure.Application.Services;

public class QueryService<T> : IQueryService<T> where T : Entity
{
    private readonly IQueryRepository<T> _repository;
    private readonly IMapper _mapper;

    public QueryService(IQueryRepository<T> repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }
    
    public async Task<TResult?> GetById<TResult>(Guid id, CancellationToken cancellationToken = default) where TResult : class
    {
        return await _repository.FirstOrDefaultAsync<TResult>(new GetByIdSpecification<T>(id), cancellationToken);
    }

    public async Task<IReadOnlyCollection<TResult>> GetAll<TResult>(CancellationToken cancellationToken = default) where TResult : class
    {
        return await _repository.ListAllAsync<TResult>(cancellationToken);
    }
}