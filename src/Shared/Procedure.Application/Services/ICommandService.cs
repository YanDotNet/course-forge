﻿using Procedure.Domain;

namespace Procedure.Application.Services;

public interface ICommandService<T> where T : Entity
{
    Task<Guid> AddAsync<TDto>(TDto dto, CancellationToken cancellationToken = default) where TDto : class;
    Task AddRangeAsync<TDto>(ICollection<TDto> dtos, CancellationToken cancellationToken = default) where TDto : class;

    Task UpdateAsync<TDto>(TDto dto, CancellationToken cancellationToken = default) where TDto : class;
    Task UpdateAsync<TDto>(ICollection<TDto> dtos, CancellationToken cancellationToken = default) where TDto : class;
  
    Task RemoveAsync(Guid id, CancellationToken cancellationToken = default);
    Task RemoveRangeAsync(ICollection<Guid> ids, CancellationToken cancellationToken = default);
}