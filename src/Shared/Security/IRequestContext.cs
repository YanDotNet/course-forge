﻿namespace Security;

public interface IRequestContext
{
    public Guid RequestId { get; set; }
}