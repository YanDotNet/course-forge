﻿namespace Security.Auth;

public interface IAuthContext
{
    public Guid Id { get; set; }
    public string Username { get; set; }
}