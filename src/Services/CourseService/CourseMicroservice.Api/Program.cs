using CourseMicroservice.Application;
using CourseMicroservice.Infrastructure;
using CourseMicroservice.Infrastructure.Persistence;
using Logging.Serilog;
using Procedure.Infrastructure.Masstransit.RabbitMq;
using DependencyInjection = Procedure.Application.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);

builder.Host.ConfigureAppConfiguration(x =>
{
    x.AddJsonFile("appsettings/appsettings.json", true, true);
});
builder.Host.ConfigureSerilogLogger("Teacher Api");

builder.Services.ConfigureInfrastructureLayer(builder.Configuration, typeof(DependencyInjection).Assembly);
builder.Services.ConfigureApplicationLayer();

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var rabbitMqConnection = RabbitMqHelper.CreateConnectionString(builder.Configuration);

builder.Services.AddHealthChecks()
    .AddDbContextCheck<CourseDbContext>()
    .AddRabbitMQ(rabbitConnectionString: rabbitMqConnection);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

// dotnet ef migrations add Init --startup-project=CourseMicroservice.Api --project=CourseMicroservice.Infrastructure --context=CourseDbContext
// dotnet ef database update --startup-project=CourseMicroservice.Api --project=CourseMicroservice.Infrastructure --context=CourseDbContext