using CourseMicroservice.Application.Models;
using CourseMicroservice.Domain;
using Microsoft.AspNetCore.Mvc;
using Procedure.Application.Services;

namespace CourseMicroservice.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class CourseController : ControllerBase
{
    [HttpPost]
    public async Task<IActionResult> Create(
        [FromServices] ICommandService<Course> service,
        [FromBody] CourseDto course,
        CancellationToken cancellationToken)
    {
        var result = await service.AddAsync(course, cancellationToken);
        return Ok(result);
    }

    [HttpGet]
    public async Task<IActionResult> Get(
        [FromServices] IQueryService<Course> service,
        CancellationToken cancellationToken)
    {
        var result = await service.GetAll<CourseDto>(cancellationToken);
        return Ok(result);
    }
    
    [HttpGet("{id:guid}")]
    public async Task<IActionResult> GetById(
        [FromServices] IQueryService<Course> service,
        [FromRoute] Guid id,
        CancellationToken cancellationToken)
    {
        var result = await service.GetById<CourseDto>(id, cancellationToken);
        return Ok(result);
    }

    [HttpPut("{id:guid}")]
    public async Task<IActionResult> Update(
        [FromServices] ICommandService<Course> service,
        [FromRoute] Guid id,
        [FromBody] CourseDto course,
        CancellationToken cancellationToken)
    {
        if (course.Id != id)
            return BadRequest();

        await service.UpdateAsync(course, cancellationToken);
        return Ok();
    }
}