using CourseMicroservice.Domain;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Procedure.Infrastructure.Persistence.EF;
using Procedure.Infrastructure.Persistence.EF.Interceptors;

namespace CourseMicroservice.Infrastructure.Persistence;

public class CourseDbContext : AppDbContext
{
    public CourseDbContext(DbContextOptions<CourseDbContext> options) : base(options)
    {
        
    }

    public DbSet<Course> Courses { get; set; } = null!;
    public DbSet<Lesson> Lessons { get; set; } = null!;
    public DbSet<Attachment> Attachments { get; set; } = null!;
    public DbSet<Stage> Stages { get; set; } = null!;
    public DbSet<StageContent> StageContents { get; set; } = null!;
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.AddOutboxMessageEntity();
        modelBuilder.AddOutboxStateEntity();
        modelBuilder.AddInboxStateEntity();
        base.OnModelCreating(modelBuilder);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.AddInterceptors(new SetUpdateTimeInterceptor());
        optionsBuilder.AddInterceptors(new SoftDeleteInterceptor());
        optionsBuilder.AddInterceptors(new UtcConvertInterceptor());
        base.OnConfiguring(optionsBuilder);
    }
}