using CourseMicroservice.Domain;
using Microsoft.EntityFrameworkCore;
using Procedure.Infrastructure.Persistence.EF;

namespace CourseMicroservice.Infrastructure.Persistence;

public class CourseReadonlyDbContext : AppReadonlyDbContext
{
    public CourseReadonlyDbContext(DbContextOptions<CourseReadonlyDbContext> options) : base(options)
    {
    }
    
    public DbSet<Course> Courses { get; set; } = null!;
    public DbSet<Lesson> Lessons { get; set; } = null!;
    public DbSet<Attachment> Attachments { get; set; } = null!;
    public DbSet<Stage> Stages { get; set; } = null!;
    public DbSet<StageContent> StageContents { get; set; } = null!;
}