﻿using System.Reflection;
using CourseMicroservice.Infrastructure.Persistence;
using CourseMicroservice.Infrastructure.Services;
using IntegrationEvents.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Procedure.Infrastructure.Masstransit.RabbitMq;
using Procedure.Infrastructure.Persistence.EF;

namespace CourseMicroservice.Infrastructure;

public static class DependencyInjection
{
    public static void ConfigureInfrastructureLayer(this IServiceCollection services, IConfiguration configuration, params Assembly[] assemblies)
    {
        services.ConfigurePersistence(configuration);
        services.ConfigureMasstransit<CourseDbContext>(configuration, assemblies);
        services.ConfigureRepositories();
        services.AddScoped<IIntegrationEventPublisher, IntegrationEventPublisher>();
    }

    private static void ConfigurePersistence(this IServiceCollection services, IConfiguration configuration)
    {
        services.ConfigureDbContext<CourseDbContext>(x => 
            x.UseNpgsql(configuration.GetConnectionString("DbConnection")));
        services.ConfigureReadonlyDbContext<CourseReadonlyDbContext>(x => 
            x.UseNpgsql(configuration.GetConnectionString("DbConnection")));
    }
}