using System.ComponentModel.DataAnnotations;
using Procedure.Domain;

namespace CourseMicroservice.Domain;

/// <summary>
/// Поток (поток = направление)
/// </summary>
public class Flow : AuditableEntity
{
    /// <summary>
    /// Название потока
    /// </summary>
    [Required]
    [MinLength(3)]
    public string Name { get; set; } = null!;

    /// <summary>
    /// Описание потока
    /// </summary>
    public string? Description { get; set; }

    /// <summary>
    /// Дата начала
    /// </summary>
    public DateOnly StartDate { get; set; }

    /// <summary>
    /// Курсы входящие в поток
    /// </summary>
    public IList<Course> Courses { get; set; } = new List<Course>();
}