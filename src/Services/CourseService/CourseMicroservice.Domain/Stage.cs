using System.ComponentModel.DataAnnotations.Schema;
using Procedure.Domain;

namespace CourseMicroservice.Domain;

/// <summary>
/// Этап урока
/// </summary>
public class Stage : AuditableEntity
{
    /// <summary>
    /// Порядковый номер этапа
    /// </summary>
    public int Index { get; set; }

    /// <summary>
    /// ID урока
    /// </summary>
    public Guid LessonId { get; set; }

    /// <summary>
    /// Урок
    /// </summary>
    [ForeignKey(nameof(LessonId))]
    public Lesson? Lesson { get; set; }
    
    /// <summary>
    /// Содержимое этапа
    /// </summary>
    public IList<StageContent> Contents { get; set; } = new List<StageContent>();
}