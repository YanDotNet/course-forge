using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Procedure.Domain;

namespace CourseMicroservice.Domain;

/// <summary>
/// Урок
/// </summary>
public class Lesson : AuditableEntity
{
    /// <summary>
    /// Название урока
    /// </summary>
    [Required]
    [MinLength(3)]
    public string Name { get; set; } = null!;

    /// <summary>
    /// Описание урока
    /// </summary>
    public string? Description { get; set; }

    /// <summary>
    /// Порядкой номер урока
    /// </summary>
    public int Index { get; set; }

    /// <summary>
    /// ID курса
    /// </summary>
    public Guid CourseId { get; set; }
    
    /// <summary>
    /// Курс
    /// </summary>
    [ForeignKey(nameof(CourseId))]
    public Course? Course { get; set; }

    /// <summary>
    /// Этапы урока
    /// </summary>
    public IList<Stage> Stages { get; set; } = new List<Stage>();

    /// <summary>
    /// Дополнительные материалы урока
    /// </summary>
    public IList<Attachment> Attachments { get; set; } = new List<Attachment>();
}