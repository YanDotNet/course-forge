using System.ComponentModel.DataAnnotations.Schema;
using Procedure.Domain;

namespace CourseMicroservice.Domain;

/// <summary>
/// Содержимое этапа
/// </summary>
public class StageContent : AuditableEntity
{
    /// <summary>
    /// Порядковый номер части этапа
    /// </summary>
    public int Index { get; set; }
    
    /// <summary>
    /// Содержимое
    /// </summary>
    public string? Content { get; set; }
    
    public Guid StageId { get; set; }
    
    [ForeignKey(nameof(StageId))]
    public Stage? Stage { get; set; }
}