using System.ComponentModel.DataAnnotations.Schema;
using CourseMicroservice.Domain.Enums;
using Procedure.Domain;

namespace CourseMicroservice.Domain;

/// <summary>
/// Материалы урока
/// </summary>
public class Attachment : AuditableEntity
{
    /// <summary>
    /// ID урока
    /// </summary>
    public Guid LessonId { get; set; }
    
    /// <summary>
    /// Тип вложения
    /// </summary>
    public AttachmentType Type { get; set; }

    /// <summary>
    /// Путь к содержимому
    /// </summary>
    public string? Path { get; set; }
    
    /// <summary>
    /// Урок
    /// </summary>
    [ForeignKey(nameof(LessonId))]
    public Lesson? Lesson { get; set; }
}