﻿using System.ComponentModel.DataAnnotations;
using Procedure.Domain;

namespace CourseMicroservice.Domain;

/// <summary>
/// Курс
/// </summary>
public class Course : AuditableEntity
{
    /// <summary>
    /// Название курса
    /// </summary>
    [Required]
    [MinLength(2)]
    public string Name { get; set; } = null!;

    /// <summary>
    /// Описание курса
    /// </summary>
    public string? Description { get; set; }

    /// <summary>
    /// Уроки в рамках курса
    /// </summary>
    public IList<Lesson> Lessons { get; set; } = new List<Lesson>();
}