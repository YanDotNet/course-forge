namespace CourseMicroservice.Domain.Enums;

public enum AttachmentType
{
    Url,
    Photo,
    Video,
    Audio
}