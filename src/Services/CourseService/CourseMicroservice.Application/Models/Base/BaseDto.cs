namespace CourseMicroservice.Application.Models.Base;

public record BaseDto
{
    public Guid Id { get; set; }
}