namespace CourseMicroservice.Application.Models.Base;

public record BaseAuditableDto : BaseDto
{
    public DateTimeOffset CreatedAt { get; set; }
    public DateTimeOffset UpdatedAt { get; set; }
}