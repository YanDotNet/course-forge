using CourseMicroservice.Application.Models.Base;
using CourseMicroservice.Domain.Enums;

namespace CourseMicroservice.Application.Models;

public record AttachmentDto : BaseAuditableDto
{
    public Guid LessonId { get; init; }
    
    public AttachmentType Type { get; init; }

    public string? Path { get; init; }
}