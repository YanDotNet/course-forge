using CourseMicroservice.Application.Models.Base;

namespace CourseMicroservice.Application.Models;

public record LessonDto : BaseAuditableDto
{
    public string Name { get; init; } = null!;

    public string? Description { get; init; }

    public int Index { get; init; }

    public Guid CourseId { get; init; }
    
    public IReadOnlyList<StageDto> Stages { get; init; } = new List<StageDto>();

    public IReadOnlyList<AttachmentDto> Attachments { get; init; } = new List<AttachmentDto>();
}