using CourseMicroservice.Application.Models.Base;

namespace CourseMicroservice.Application.Models;

public record FlowDto : BaseAuditableDto
{
    public string Name { get; set; } = null!;

    public string? Description { get; set; }

    public DateOnly StartDate { get; set; }

    public IList<CourseDto> Courses { get; set; } = new List<CourseDto>();
}