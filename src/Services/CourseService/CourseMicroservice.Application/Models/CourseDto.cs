using CourseMicroservice.Application.Models.Base;

namespace CourseMicroservice.Application.Models;

public record CourseDto : BaseAuditableDto
{
    public string? Name { get; init; }
    
    public string? Description { get; init; }

    public IReadOnlyList<LessonDto> Lessons { get; init; } = new List<LessonDto>();
}