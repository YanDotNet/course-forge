using CourseMicroservice.Application.Models.Base;

namespace CourseMicroservice.Application.Models;

public record StageDto : BaseAuditableDto
{
    public int Index { get; init; }

    public Guid LessonId { get; init; }

    public IReadOnlyList<StageContentDto> Contents { get; init; } = new List<StageContentDto>();
}