using CourseMicroservice.Application.Models.Base;

namespace CourseMicroservice.Application.Models;

public record StageContentDto : BaseAuditableDto
{
    public int Index { get; init; }
    
    public string? Content { get; init; }
    
    public Guid StageId { get; init; }
}