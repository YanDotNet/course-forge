﻿using Microsoft.Extensions.DependencyInjection;
using Procedure.Application;

namespace CourseMicroservice.Application;

public static class DependencyInjection
{
    public static void ConfigureApplicationLayer(this IServiceCollection services)
    {
        services.AddAutoMapper(typeof(DependencyInjection).Assembly);
        services.ConfigureServices();
    }
}