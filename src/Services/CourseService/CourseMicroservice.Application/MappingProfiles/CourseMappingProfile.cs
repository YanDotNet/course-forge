using AutoMapper;
using CourseMicroservice.Application.Models;
using CourseMicroservice.Domain;

namespace CourseMicroservice.Application.MappingProfiles;

public class CourseMappingProfile : Profile
{
    public CourseMappingProfile()
    {
        CreateMap<Course, CourseDto>().ReverseMap();
        CreateMap<Lesson, LessonDto>().ReverseMap();
        CreateMap<Stage, StageDto>().ReverseMap();
        CreateMap<StageContent, StageContentDto>().ReverseMap();
        CreateMap<Attachment, AttachmentDto>().ReverseMap();
        
    }
}