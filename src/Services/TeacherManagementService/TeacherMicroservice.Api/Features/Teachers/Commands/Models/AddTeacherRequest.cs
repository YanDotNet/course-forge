namespace TeacherMicroservice.Api.Features.Teachers.Commands.Models;

public record AddTeacherRequest(
    string? FirstName, 
    string? LastName, 
    string? Patronymic,
    string? Email,
    string PhoneNumber);