namespace TeacherMicroservice.Api.Features.Teachers.Commands.Models;

public record AddTeacherResponse(Guid Id);