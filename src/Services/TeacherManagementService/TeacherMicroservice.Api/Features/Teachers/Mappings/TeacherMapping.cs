using AutoMapper;
using TeacherMicroservice.Api.Features.Teachers.Commands.Models;
using TeacherMicroservice.Api.Features.Teachers.Entities;
using TeacherMicroservice.Api.Features.Teachers.Queries.Models;

namespace TeacherMicroservice.Api.Features.Teachers.Mappings;

public class TeacherMapping : Profile
{
    public TeacherMapping()
    {
        CreateMap<AddTeacherRequest, Teacher>();

        CreateMap<Teacher, TeacherDto>();
    }
}