using Infrastructure.EF.Auditing.Extensions;
using Infrastructure.EF.Auditing.Interceptors;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Procedure.Infrastructure.Persistence.EF;
using Procedure.Infrastructure.Persistence.EF.Interceptors;
using TeacherMicroservice.Api.Features.Teachers.Entities;

namespace TeacherMicroservice.Api.Features.Persistence;

public class TeacherDbContext : AppDbContext
{
    public TeacherDbContext(DbContextOptions options) : base(options)
    {
    }

    public DbSet<Teacher> Teachers { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.AddOutboxMessageEntity();
        modelBuilder.AddOutboxStateEntity();
        modelBuilder.AddInboxStateEntity();
        modelBuilder.EnableAuditHistory();
        base.OnModelCreating(modelBuilder);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.AddInterceptors(
            new SetUpdateTimeInterceptor(),
            new SoftDeleteInterceptor(),
            new UtcConvertInterceptor());

        base.OnConfiguring(optionsBuilder);
    }
}