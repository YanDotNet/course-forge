using Microsoft.EntityFrameworkCore;
using Procedure.Infrastructure.Persistence.EF;
using TeacherMicroservice.Api.Features.Teachers.Entities;

namespace TeacherMicroservice.Api.Features.Persistence;

public sealed class TeacherReadonlyDbContext : AppReadonlyDbContext
{
    public TeacherReadonlyDbContext(DbContextOptions options) : base(options)
    {
        ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
    }

    public DbSet<Teacher> Teachers { get; set; } = null!;
}