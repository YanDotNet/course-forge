using System.Reflection;
using TeacherMicroservice.Api.Features.Persistence;
using IntegrationEvents.Contracts;
using Logging.Serilog;
using Microsoft.EntityFrameworkCore;
using Procedure.Application;
using Procedure.Infrastructure.Persistence.EF;
using TeacherMicroservice.Api.Features.Services;
using DependencyInjection = Procedure.Infrastructure.Masstransit.RabbitMq.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);

builder.Host.ConfigureAppConfiguration(x =>
{
    x.AddJsonFile("appsettings/appsettings.json", true, true);
});
builder.Host.ConfigureSerilogLogger("Teacher Api");

builder.Services.ConfigureDbContext<TeacherDbContext>(x => 
    x.UseNpgsql(builder.Configuration.GetConnectionString("DbConnection")));
builder.Services.ConfigureReadonlyDbContext<TeacherReadonlyDbContext>(x => 
    x.UseNpgsql(builder.Configuration.GetConnectionString("DbConnection")));

DependencyInjection.ConfigureMasstransit(builder.Services, builder.Configuration);

builder.Services.ConfigureRepositories();
builder.Services.ConfigureServices();
builder.Services.AddScoped<IIntegrationEventPublisher, IntegrationEventPublisher>();
builder.Services.AddAutoMapper(Assembly.GetExecutingAssembly());

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

// dotnet ef migrations add AddAuditing --project=TeacherMicroservice.Api --context=TeacherDbContext -o Features/Persistence/Migrations
// dotnet ef database update --project=TeacherMicroservice.Api --context=TeacherDbContext