using Microsoft.AspNetCore.Mvc;
using Procedure.Application.Services;
using TeacherMicroservice.Api.Features.Teachers.Commands.Models;
using TeacherMicroservice.Api.Features.Teachers.Entities;
using TeacherMicroservice.Api.Features.Teachers.Queries.Models;

namespace TeacherMicroservice.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TeachersController : ControllerBase
{
    [HttpPost]
    public async Task<IActionResult> AddTeacher(
        [FromServices] ICommandService<Teacher> commandService,
        [FromBody] AddTeacherRequest request,
        CancellationToken cancellationToken)
    {
        var result = await commandService.AddAsync(request, cancellationToken);
        return Ok(new AddTeacherResponse(result));
    }
    
    [HttpGet("{id:guid}")]
    public async Task<IActionResult> GetById(
        [FromServices] IQueryService<Teacher> queryService,
        [FromRoute] Guid id,
        CancellationToken cancellationToken)
    {
        var result = await queryService.GetById<TeacherDto>(id, cancellationToken);
        return Ok(result);
    }
}