﻿using IntegrationEvents.Contracts;

namespace TeacherMicroservice.IntegrationEvents;

public record TeacherCreatedIntegrationEvent : IIntegrationEvent
{
    public Guid Id { get; init; }

    public DateTimeOffset CreatedAt { get; init; }

    public string? FirstName { get; init; }

    public string? LastName { get; init; }
    
    public string? Patronymic { get; init; }

    public string? Email { get; init; }

    public string? PhoneNumber { get; init; }
}