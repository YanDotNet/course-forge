using Microsoft.AspNetCore.Mvc;
using Procedure.Application.Services;
using StudentMicroservice.Api.Features.Students.Commands;
using StudentMicroservice.Api.Features.Students.Commands.Models;
using StudentMicroservice.Api.Features.Students.Entities;
using StudentMicroservice.Api.Features.Students.Queries;

namespace StudentMicroservice.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class StudentsController : ControllerBase
{
    private readonly IStudentCommandService _commandService;
    private readonly IQueryService<Student> _queryService;
    private readonly ILogger<StudentsController> _logger;

    public StudentsController(IStudentCommandService commandService,
        IQueryService<Student> queryService,
        ILogger<StudentsController> logger)
    {
        _commandService = commandService;
        _queryService = queryService;
        _logger = logger;
    }
    
    [HttpPost]
    public async Task<IActionResult> Create(
        [FromBody] CreateStudentCommand request, CancellationToken cancellationToken)
    {
        var response = await _commandService.CreateStudent(request, cancellationToken);
        return Ok(response);
    }

    [HttpGet("{id:guid}")]
    public async Task<IActionResult> GetById(
        [FromRoute] Guid id,
        CancellationToken cancellationToken)
    {
        var response = await _queryService.GetById<GetStudentByIdResponse>(id, cancellationToken);
        return Ok(response);
    }

    [HttpGet]
    public async Task<IActionResult> Get(CancellationToken cancellationToken)
    {
        _logger.LogInformation("test");
        var response = await _queryService.GetAll<GetStudentByIdResponse>(cancellationToken);
        return Ok(response);
    }
}