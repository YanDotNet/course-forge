using System.Security.Claims;
using Security.Auth;

namespace StudentMicroservice.Api.Features.Services;

public class AuthContext : IAuthContext
{
    public AuthContext(IHttpContextAccessor contextAccessor)
    {
        Id = Guid.Parse(contextAccessor.HttpContext?.User.FindFirst(ClaimTypes.NameIdentifier)?.Value ?? "");
        Username = contextAccessor.HttpContext?.User?.Identity?.Name ?? "";
    }
    
    public Guid Id { get; set; }
    public string Username { get; set; }
}