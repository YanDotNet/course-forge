using StudentMicroservice.Api.Features.Students.Commands;
using AutoMapper;
using StudentMicroservice.Api.Features.Students.Commands.Models;
using StudentMicroservice.Api.Features.Students.Entities;
using StudentMicroservice.Api.Features.Students.Queries;

namespace StudentMicroservice.Api.Features.Students.Mappings;

public class StudentMappingProfile : Profile
{
    public StudentMappingProfile()
    {
        CreateMap<CreateStudentCommand, Student>();
        CreateMap<Student, GetStudentByIdResponse>();
    }
}