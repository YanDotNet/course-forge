namespace StudentMicroservice.Api.Features.Students.Queries;

public record GetStudentByIdQuery(Guid Id);