using StudentMicroservice.IntegrationEvents;
using StudentMicroservice.Api.Features.Students.Entities;

namespace StudentMicroservice.Api.Features.Students.IntegrationEvents;

public static class IntegrationEventFactory
{
    public static StudentCreatedIntegrationEvent GetStudentCreatedEvent(Student student)
    {
        return new StudentCreatedIntegrationEvent
        {
            Id = student.Id,
            CreatedAt = student.CreatedAt,
            Email = student.Email,
            FirstName = student.FirstName,
            LastName = student.LastName,
            Patronymic = student.Patronymic,
            PhoneNumber = student.PhoneNumber
        };
    }
}