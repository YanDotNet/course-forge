using AutoMapper;
using IntegrationEvents.Contracts;
using Procedure.Application.Repositories;
using StudentMicroservice.Api.Features.Students.Commands.Models;
using StudentMicroservice.Api.Features.Students.Entities;
using StudentMicroservice.Api.Features.Students.IntegrationEvents;

namespace StudentMicroservice.Api.Features.Students.Commands;

public class StudentCommandService : IStudentCommandService
{
    private readonly IAsyncRepository<Student> _repository;
    private readonly IMapper _mapper;
    private readonly IIntegrationEventPublisher _eventBus;

    public StudentCommandService(IAsyncRepository<Student> repository,
        IMapper mapper,
        IIntegrationEventPublisher eventBus)
    {
        _repository = repository;
        _mapper = mapper;
        _eventBus = eventBus;
    }
    
    public async Task<CreateStudentResponse> CreateStudent(CreateStudentCommand command, CancellationToken cancellationToken = default)
    {
        var student = _mapper.Map<Student>(command);

        await _eventBus.PublishAsync(IntegrationEventFactory.GetStudentCreatedEvent(student), cancellationToken);

        await _repository.AddAsync(student, cancellationToken);
        
        return new CreateStudentResponse(student.Id);
    }
}