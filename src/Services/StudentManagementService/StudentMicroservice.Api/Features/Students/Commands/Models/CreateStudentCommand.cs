namespace StudentMicroservice.Api.Features.Students.Commands.Models;

public record CreateStudentCommand(
    string? FirstName, 
    string? LastName, 
    string? Patronymic,
    string? Email,
    string PhoneNumber);