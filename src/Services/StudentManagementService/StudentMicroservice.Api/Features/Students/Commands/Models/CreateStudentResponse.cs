namespace StudentMicroservice.Api.Features.Students.Commands.Models;

public record CreateStudentResponse(Guid Id);