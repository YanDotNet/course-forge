using StudentMicroservice.Api.Features.Students.Commands.Models;

namespace StudentMicroservice.Api.Features.Students.Commands;

public interface IStudentCommandService
{
    Task<CreateStudentResponse> CreateStudent(CreateStudentCommand command,
        CancellationToken cancellationToken = default);
}