using System.ComponentModel.DataAnnotations;
using Procedure.Domain;

namespace StudentMicroservice.Api.Features.Students.Entities;

public class Student : AuditableEntity
{
    public string? FirstName { get; set; }

    public string? LastName { get; set; }
    
    public string? Patronymic { get; set; }

    [EmailAddress]
    public string? Email { get; set; }

    [Phone]
    [Required] 
    public string PhoneNumber { get; set; } = null!;
}