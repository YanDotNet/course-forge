using Microsoft.EntityFrameworkCore;
using Procedure.Infrastructure.Persistence.EF;
using StudentMicroservice.Api.Features.Students.Entities;

namespace StudentMicroservice.Api.Features.Persistence;

public class StudentReadonlyDbContext : AppReadonlyDbContext
{
    public StudentReadonlyDbContext(DbContextOptions<StudentReadonlyDbContext> options) : base(options)
    {
    }
    
    public DbSet<Student> Students { get; set; } = null!;
}