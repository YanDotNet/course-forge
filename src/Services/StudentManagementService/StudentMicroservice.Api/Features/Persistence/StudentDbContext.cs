using Infrastructure.EF.Auditing.Extensions;
using Infrastructure.EF.Auditing.Interceptors;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Procedure.Infrastructure.Persistence.EF;
using Procedure.Infrastructure.Persistence.EF.Interceptors;
using StudentMicroservice.Api.Features.Students.Entities;

namespace StudentMicroservice.Api.Features.Persistence;

public class StudentDbContext : AppDbContext
{
    public StudentDbContext(DbContextOptions<StudentDbContext> options) : base(options)
    {
        
    }

    public DbSet<Student> Students { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.AddOutboxMessageEntity();
        modelBuilder.AddOutboxStateEntity();
        modelBuilder.AddInboxStateEntity();
        modelBuilder.EnableAuditHistory();
        base.OnModelCreating(modelBuilder);
    }
}