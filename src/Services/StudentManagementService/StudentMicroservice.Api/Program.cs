using System.Reflection;
using IntegrationEvents.Contracts;
using Logging.Serilog;
using StudentMicroservice.Api.Extensions;
using StudentMicroservice.Api.Features.Persistence;
using StudentMicroservice.Api.Features.Services;
using StudentMicroservice.Api.Features.Students.Commands;
using Procedure.Application;
using Procedure.Infrastructure.Masstransit.RabbitMq;
using Procedure.Infrastructure.Persistence.EF;
using Security.Auth;

var builder = WebApplication.CreateBuilder(args);

builder.Host.ConfigureAppConfiguration(x =>
{
    x.AddJsonFile("appsettings/appsettings.json", true, true);
});
builder.Host.ConfigureSerilogLogger("Student Api");


builder.Services.ConfigureDb(builder.Configuration);
builder.Services.AddScoped<IAuthContext, AuthContext>();
builder.Services.ConfigureMasstransit<StudentDbContext>(builder.Configuration, Assembly.GetExecutingAssembly());

builder.Services.ConfigureRepositories();
builder.Services.ConfigureServices();
builder.Services.AddScoped<IStudentCommandService, StudentCommandService>();
builder.Services.AddScoped<IIntegrationEventPublisher, IntegrationEventPublisher>();
builder.Services.ConfigureAutoMapper();

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

// dotnet ef migrations add AddAuditing --project=StudentMicroservice.Api --context=StudentDbContext -o Features/Persistence/Migrations
// dotnet ef database update --project=StudentMicroservice.Api --context=StudentDbContext