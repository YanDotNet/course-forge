using System.Reflection;
using Infrastructure.EF.Auditing.Interceptors;
using Microsoft.EntityFrameworkCore;
using Procedure.Infrastructure.Persistence.EF;
using Procedure.Infrastructure.Persistence.EF.Interceptors;
using StudentMicroservice.Api.Features.Persistence;

namespace StudentMicroservice.Api.Extensions;

public static class DependencyInjectionExtensions
{
    public static void ConfigureDb(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<LocalAuditInterceptor>();
        
        services.ConfigureDbContext<StudentDbContext>((sp, options) =>
        {
            options.UseNpgsql(configuration.GetConnectionString("DbConnection"))
                .AddInterceptors(
                    sp.GetRequiredService<LocalAuditInterceptor>(),
                    new SetUpdateTimeInterceptor(),
                    new SoftDeleteInterceptor(),
                    new UtcConvertInterceptor());
        });
        services.ConfigureReadonlyDbContext<StudentReadonlyDbContext>(x => 
            x.UseNpgsql(configuration.GetConnectionString("DbConnection")));
    }
    
    public static void ConfigureAutoMapper(this IServiceCollection services)
    {
        services.AddAutoMapper(Assembly.GetExecutingAssembly());
    }
}